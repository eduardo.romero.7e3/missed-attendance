package cat.itb.gestitb;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.Calendar;
import java.util.Date;

public class MissedAttendanceFragment extends Fragment {
    EditText studentNameText;
    Button dateButton, addButton;
    Spinner mSpinner;
    CheckBox justifiedBox;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.missed_attendance_fragment, container, false);

        studentNameText = v.findViewById(R.id.editTextTextPersonName);
        dateButton = v.findViewById(R.id.dateButton);
        addButton = v.findViewById(R.id.addButton);
        mSpinner = v.findViewById(R.id.mSpinner);
        justifiedBox = v.findViewById(R.id.justifiedBox);

        final Date date = Calendar.getInstance().getTime();
        dateButton.setText(date.toString());

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MissedAttendanceModel missedAttendance = new MissedAttendanceModel(
                        studentNameText.getText().toString(),
                        mSpinner.getSelectedItem().toString(),
                        date,
                        justifiedBox.isChecked()

                );

                final AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
                dialog.setTitle(R.string.dialogTitle);

                StringBuilder messageBuilder = new StringBuilder();
                messageBuilder.append("The student ").append(missedAttendance.getName());
                messageBuilder.append(" has missed ").append(missedAttendance.getModule());
                messageBuilder.append(" on ").append(date.toString());
                if(missedAttendance.isJustified()) messageBuilder.append(" with justification.");
                else messageBuilder.append(" without justification.");

                dialog.setMessage(messageBuilder.toString());

                dialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        studentNameText.setText("");
                        mSpinner.setSelection(0);
                        justifiedBox.setChecked(true);
                        dateButton.setText(Calendar.getInstance().getTime().toString());
                    }
                });

                dialog.show();

            }
        });

        return v;
    }
}
