package cat.itb.gestitb;

import java.util.Date;

public class MissedAttendanceModel {
    private String name;
    private String module;
    private Date date;
    private boolean justified;

    public MissedAttendanceModel(String name, String module, Date date, boolean justified) {
        this.name = name;
        this.module = module;
        this.date = date;
        this.justified = justified;
    }

    public String getName() {
        return name;
    }

    public String getModule() {
        return module;
    }

    public Date getDate() {
        return date;
    }

    public boolean isJustified() {
        return justified;
    }
}
