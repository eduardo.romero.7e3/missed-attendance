package cat.itb.gestitb;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Fragment missedAttendance = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
        if(missedAttendance == null){
            missedAttendance = new MissedAttendanceFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.fragmentContainer, missedAttendance).commit();
        }
    }
}